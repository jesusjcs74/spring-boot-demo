package mx.gob.sat.avl.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoApplication {
    
	@Autowired
        private Environment env;
        
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
       
		String info = "Info: Servicio arriba v.1.0.0";
        System.out.println(info);
	}
        
        @GetMapping("avl")
        public String saludar(){
            System.out.println("Info: Se consume el servicio.");
            String info = env.getProperty("CONEXION");
            String cerfica = env.getProperty("Certificado");
            String mensaje = "Bienvenido al Demo de AVL, si puedes visualizar éste mensaje, el servicio esta arriba!-v1.0.1 - Variable de Entorno: "+info+" cerficia:"+cerfica;
            
            return mensaje;     
        }

}
