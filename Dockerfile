FROM openjdk:8u111-jdk-alpine
VOLUME /tmp 
ADD /target/*.jar docker-spring-boot.jar
ENTRYPOINT [ "java","-jar","docker-spring-boot.jar" ]
